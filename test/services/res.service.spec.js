const chai = require('chai');
const rest = require('./../../services/rest.service');
const config = require('./../../config');
describe('REST SERVICE', () => {
  describe('GET', () => {
    it('OK', (done) => {
      rest.get(config.urls.pokemon)
        .then((data) => {
          chai.expect(data).to.have.property('count');
          done();
        });
    });
  });
});
