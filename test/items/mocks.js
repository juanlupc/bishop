const mocks = {
  pokemonList: {
    count: 949,
    next: 'https://pokeapi-215911.firebaseapp.com/api/v2/pokemon?offset=20&limit=20',
    previous: null,
    results: [
      {
        name: 'bulbasaur',
        url: 'https://pokeapi.co/api/v2/pokemon/1/'
      },
      {
        name: 'ivysaur',
        url: 'https://pokeapi.co/api/v2/pokemon/2/'
      }
    ]
  },
  driverList: {
    MRData: {
      _attributes: {
        xmlns: 'http://ergast.com/mrd/1.4',
        series: 'f1',
        url: 'http://ergast.com/api/f1/drivers',
        limit: '30',
        offset: '0',
        total: '847'
      },
      DriverTable: {
        Driver: [
          {
            _attributes: {
              driverId: 'abate',
              url: 'http://en.wikipedia.org/wiki/Carlo_Mario_Abate'
            },
            GivenName: {
              _text: 'Carlo'
            },
            FamilyName: {
              _text: 'Abate'
            },
            DateOfBirth: {
              _text: '1932-07-10'
            },
            Nationality: {
              _text: 'Italian'
            }
          },
          {
            _attributes: {
              driverId: 'abecassis',
              url: 'http://en.wikipedia.org/wiki/George_Abecassis'
            },
            GivenName: {
              _text: 'George'
            },
            FamilyName: {
              _text: 'Abecassis'
            },
            DateOfBirth: {
              _text: '1913-03-21'
            },
            Nationality: {
              _text: 'British'
            }
          }
        ]
      }
    }
  },
  cardList: [
    {
      _id: '5b099537ab411c001423ec3b',
      idName: 'arrows',
      rarity: 'Common',
      type: 'Spell',
      name: 'Arrows',
      description: 'Arrows pepper a large area, damaging all enemies hit. Reduced damage to Crown Towers.',
      elixirCost: 3,
      copyId: 28000001,
      arena: 0,
      order: 0,
      __v: 0
    },
    {
      _id: '5b099537ab411c001423ec3c',
      idName: 'bomber',
      rarity: 'Common',
      type: 'Troop',
      name: 'Bomber',
      description: 'Small, lightly protected skeleton that throws bombs. Deals area damage that can wipe out a swarm of enemies.',
      elixirCost: 3,
      copyId: 26000013,
      arena: 0,
      order: 1,
      __v: 0
    }
  ],
  id: {
    ok: 'ok',
    ko: 'ko'
  },
  pokemon: {
    id: 2,
    name: 'ivysaur',
    sprites: {
      back_default: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/2.png'
    }
  },
  driver: {
    MRData: {
      _attributes: {
        xmlns: 'http://ergast.com/mrd/1.4',
        series: 'f1',
        url: 'http://ergast.com/api/f1/drivers/alonso',
        limit: '30',
        offset: '0',
        total: '1'
      },
      DriverTable: {
        _attributes: {
          driverId: 'alonso'
        },
        Driver: {
          _attributes: {
            driverId: 'alonso',
            code: 'ALO',
            url: 'http://en.wikipedia.org/wiki/Fernando_Alonso'
          },
          PermanentNumber: {
            _text: '14'
          },
          GivenName: {
            _text: 'Fernando'
          },
          FamilyName: {
            _text: 'Alonso'
          },
          DateOfBirth: {
            _text: '1981-07-29'
          },
          Nationality: {
            _text: 'Spanish'
          }
        }
      }
    }
  },
  card: {
    _id: '5b099537ab411c001423ec59',
    idName: 'inferno-tower',
    rarity: 'Rare',
    type: 'Building',
    name: 'Inferno Tower',
    description: 'Defensive building, roasts targets for damage that increases over time. Burns through even the biggest and toughest enemies!',
    elixirCost: 5,
    copyId: 27000003,
    arena: 4,
    order: 32,
    __v: 0
  }
};

module.exports = mocks;
