const sinon = require('sinon');
const chai = require('chai');
const rest = require('./../../services/rest.service');
const soap = require('./../../services/soap.service');
const config = require('./../../config');
const logic = require('./../../controllers/items.logic');
const mock = require('./mocks');

describe('ITEMS LOGIC', ()=> {
  describe('Get Items', () => {
    let stubRest, stubSoap ;
    beforeEach((done) => {
      stubRest = sinon.stub(rest, 'get');
      stubSoap = sinon.stub(soap, 'get');
      done();
    });
    afterEach((done) => {
      rest.get.restore();
      soap.get.restore();
      done();
    });
    it('OK', (done) => {
      stubRest.withArgs(`${config.urls.cards.uriBase}${config.urls.cards.api}`).resolves(mock.cardList);
      stubSoap.withArgs(config.urls.drivers).resolves(mock.driverList);
      stubRest.withArgs(`${config.urls.pokemon}?limit=1000`).resolves(mock.pokemonList);
      logic.getItems()
        .then((data) => {
          chai.expect(data).to.be.an('array');
          chai.expect(data[0]).to.have.property('id');
          done();
        });
    });
    it('KO', (done) => {
      stubRest.withArgs(`${config.urls.cards.uriBase}${config.urls.cards.api}`).rejects({ message: 'Error'});
      stubSoap.withArgs(config.urls.drivers).rejects({ message: 'Error'});
      stubRest.withArgs(`${config.urls.pokemon}?limit=1000`).rejects({ message: 'Error'});
      logic.getItems()
        .catch((e) => {
          try {
            chai.expect(e.message).to.equal('Error');
            done();
          }
          catch (e) {
            done();
          }
        });
    });
  });
  describe('Get Item By ID', () => {
    let stubRest, stubSoap ;
    beforeEach((done) => {
      stubRest = sinon.stub(rest, 'get');
      stubSoap = sinon.stub(soap, 'get');
      done();
    });
    afterEach((done) => {
      rest.get.restore();
      soap.get.restore();
      done();
    });
    it('OK', (done) => {
      stubRest.withArgs(`${config.urls.cards.uriBase}${config.urls.cards.api}${mock.id.ok}`).resolves(mock.card);
      stubSoap.withArgs(`${config.urls.drivers}/${mock.id.ok}`).resolves(mock.driver);
      stubRest.withArgs(`${config.urls.pokemon}${mock.id.ok}`).resolves(mock.pokemon);
      logic.getItem(mock.id.ok)
        .then((data) => {
          chai.expect(data).to.be.an('object');
          chai.expect(data).to.have.property('id');
          done();
        });
    });
    it('KO', (done) => {
      stubRest.withArgs(`${config.urls.cards.uriBase}${config.urls.cards.api}${mock.id.ko}`).rejects({ message: 'Error'});
      stubSoap.withArgs(`${config.urls.drivers}/${mock.id.ko}`).resolves({driver: {MRData: {DriverTable: {}}}});
      stubRest.withArgs(`${config.urls.pokemon}${mock.id.ko}`).rejects({ message: 'Error'});

      logic.getItem(mock.id.ko)
        .catch((e) => {
          try {
            chai.expect(e.message).to.equal('Error');
            done();
          }
          catch (e) {
            done();
          }
        });
    });
  });


});
