const config = {
  urls: {
    cards: {
      uriBase:'http://www.clashapi.xyz/',
      api: 'api/cards/',
      images: 'images/cards/'
    },
    pokemon: 'https://pokeapi.co/api/v2/pokemon/',
    drivers: 'http://ergast.com/api/f1/drivers'
  },
  redis: {
    name: 'redis',
    host: 'redis',
    port: 6379,
    cacheTimeExpires: 3000,
    dbIndex: 7,
    options: {
      max_attempts: 10,
      connect_timeout: 3000,
      socket_nodelay: true
    }
  }
};

module.exports = config;
