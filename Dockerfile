FROM mhart/alpine-node:10.15.0
# MS Logic
RUN mkdir /app
WORKDIR /app
COPY . /app
EXPOSE 3000
CMD npm start
