const app = require('./app');
const redis = require('redis');
const apicache = require('apicache');
const config = require('./config');

let cacheWithRedis = apicache.options({ redisClient: redis.createClient(config.redis) }).middleware;

app
  .use(cacheWithRedis('1 seconds'))
  .use('/items', require('./controllers/items'))
  .all('*', (req, res) => res.sendStatus(404))
  .listen(3000);
