# BISHOP <juanluperez@hotmail>

[![JavaScript Style Guide](https://img.shields.io/badge/style-eslint-blue.svg)](https://eslint.org/)
[![ASDFAS](https://img.shields.io/badge/node-6.9.5-brightgreen.svg)](https://nodejs.org/es/)
[![NPM Version](https://img.shields.io/badge/npm-8.11.3-red.svg)](https://www.npmjs.com/)
[![Docker Version](https://img.shields.io/badge/docker-18.06.1-blue.svg)](https://www.docker.com/)



The technologies used are:
* node.js
* express
* mocha
* Docker
* Redis

You need to have Docker installed as well.

### Prerequisites
- [Git](https://git-scm.com/)
- [Docker](https://www.docker.com/) Docker & Docker compose
- [Npm](nodejs.org) npm ^8.11.3
In order to interact with the module:
````bash
    $ chmod +x ./do.sh
    $ ./do.sh help        // Show a menu with all the options
    $ ./do.sh build       // npm install & docker build
    $ ./do.sh start       // Run the module
````
