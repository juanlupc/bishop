const { Router } = require('express');
const logic = require('./items.logic');

module.exports = new Router()
  .get('/', (req, res) => {
    return logic.getItems()
      .then((data) => {
        return  res.status(200).json(data);
      })
      .catch((e) => {
        console.error(e, JSON.stringify(e));
        res.status(e.statusCode).json({ error: e.message});
      });

  })
  .get('/:id', (req, res) => {
    return logic.getItem(req.params.id)
      .then((data) => {
        return  res.status(200).json(data);
      })
      .catch((e) => {
        console.error(e, JSON.stringify(e));
        res.status(e.statusCode).json({ error: e.message});
      });

  });
