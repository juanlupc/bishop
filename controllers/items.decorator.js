const cardsUrl = require('./../config').urls.cards;

const pokemonsToItems = (pokemons) => {
  return pokemons.results.map((pokemon) => {
    return {
      id: Number(pokemon.url.split('/')[pokemon.url.split('/').length - 2]),
      name: pokemon.name
    };
  });
};

const driversToItems = (drivers) => {
  return drivers.MRData.DriverTable.Driver.map((driver) =>{
    return {
      id: driver._attributes.driverId,
      name: `${driver.GivenName._text} ${driver.FamilyName._text}`
    };
  });
};

const cardsToItems = (cards) => {
  return cards.map((card) => {
    return {
      id: card._id,
      name: card.name
    };
  });
};

const driverToItem = (driver) => {
  const properties  =  driver.MRData.DriverTable.Driver;
  if (!properties) {
    return false;
  }
  return {
    id: properties._attributes.driverId,
    image: properties._attributes.url,
    name: `${properties.GivenName._text} ${properties.FamilyName._text}`
  };
};

const pokemonToItem = (pokemon) => {
  if (!pokemon) {
    return false;
  }
  return {
    id: pokemon.id,
    image: pokemon.sprites.back_default,
    name: pokemon.name
  };
};
const cardToItem = (card) => {
  if (!card) {
    return false;
  }
  return {
    id: card._id,
    image: `${cardsUrl.uriBase}${cardsUrl.images}${card.idName}.png`,
    name: card.name
  };
};

const getOK = (results) => {
  let ok = false;
  results.forEach(item => {
    if(item) {
      ok = item;
    }
  });
  return ok;
};

module.exports = {
  driversToItems,
  pokemonsToItems,
  cardsToItems,
  driverToItem,
  pokemonToItem,
  cardToItem,
  getOK
};
