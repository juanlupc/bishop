const config = require('./../config')
const rest = require('./../services/rest.service')
const soap = require('./../services/soap.service')
const decorator = require('./items.decorator')
const createError = require('http-errors');


const getItems = async () => {
  try {
    const driversRequest = await soap.get(config.urls.drivers);
    const driversToItems = decorator.driversToItems(driversRequest);
    const pokemonRequest = await rest.get(`${config.urls.pokemon}?limit=1000`);
    const pokemonToItems = decorator.pokemonsToItems(pokemonRequest);
    const cardsRequest = await rest.get(`${config.urls.cards.uriBase}${config.urls.cards.api}`);
    const cardsToItems = decorator.cardsToItems(cardsRequest)
    const union = pokemonToItems.concat(cardsToItems).concat(driversToItems)
    return union
  } catch (e) {
    throw e
  }
}

const getItem = async (id) => {
  try {
    const driversRequest = await soap.get(`${config.urls.drivers}/${id}`)
    const driverToItem = decorator.driverToItem(driversRequest);
    const cardsRequest = await rest.get(`${config.urls.cards.uriBase}${config.urls.cards.api}${id}`).catch(() => false);
    const cardToItem = decorator.cardToItem(cardsRequest)
    const pokemonRequest = await rest.get(`${config.urls.pokemon}${id}`).catch(() => false);
    const pokemonToItem = decorator.pokemonToItem(pokemonRequest);
    const result =  decorator.getOK([driverToItem,pokemonToItem,cardToItem])
    if(result) {
      return result
    } else {
      throw createError(404, 'Not Found')
    }
  } catch (e) {
    throw e
  }
}

module.exports = {
  getItems,
  getItem
}
