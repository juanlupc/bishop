const createError = require('http-errors');
const client = require('superagent');

function get(url) {
  return new Promise((resolve, reject) => {
    client
      .get(url)
      .set('Accept', 'application/json')
      .end((err, resp) => {
        if (err) {
          return reject(createError(err.status, 'Rest Service'));
        }
        return resolve(resp.body);
      });
  });
}

module.exports = {
  get
};
