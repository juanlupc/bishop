const createError = require('http-errors');
const client = require('superagent');
const convert = require('xml-js');

function get(url) {
  return new Promise((resolve, reject) => {
    client
      .get(url)
      .buffer()
      .type('xml')
      .end((err, resp) => {
        if (err) {
          reject(createError(500, 'Server Error'));
        }
        const xml = resp.text;
        resolve(convert.xml2js(xml, {compact: true, spaces: 4}));
      });
  });
}

module.exports = {
  get
};
